package test.java.com.slice.models;

import main.java.com.slice.models.Train;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class TrainTest {
    private static Train cut;

    private static Train train1 = new Train("a", 1, 1, 1);
    private static Train train2 = new Train("b", 2, 2, 2);
    private static Train train3 = new Train("a", 1 , 1, 1);

    static Arguments[] toStringTestArgs(){
        return new Arguments[]{
                Arguments.arguments(train1,
                  "Train: destination = 'a', trainNumber = 1, departureTime = 1.0, placesNumber = 1."),
                Arguments.arguments(train2,
                  "Train: destination = 'b', trainNumber = 2, departureTime = 2.0, placesNumber = 2."),
        };
    }

    @ParameterizedTest
    @MethodSource("toStringTestArgs")
    void toStringTest(Train train, String expected){
        cut = train;
        String actual = cut.toString();
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] equalsTestArgs(){
        return new Arguments[]{
                Arguments.arguments(train1, train2, false),
                Arguments.arguments(train1, train3, true)
        };
    }

    @ParameterizedTest
    @MethodSource("equalsTestArgs")
    void equalsTest(Train train1, Train train2, boolean expected){
        cut = train1;
        boolean actual = cut.equals(train2);
        Assertions.assertEquals(expected, actual);
    }
}
