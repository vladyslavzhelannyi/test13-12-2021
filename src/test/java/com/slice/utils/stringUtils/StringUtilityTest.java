package test.java.com.slice.utils.stringUtils;

import main.java.com.slice.utils.stringUtils.StringUtility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.regex.Matcher;

public class StringUtilityTest {
    private static final StringUtility cut = new StringUtility();

    static Arguments[] isHexadecimalTestArgs(){
        return new Arguments[]{
          Arguments.arguments("0x12AF3", true),
          Arguments.arguments("12233", false)
        };
    }

    static Arguments[] isPhoneNumberTestArgs(){
        return new Arguments[]{
                Arguments.arguments("+380 95 193-22-33", true),
                Arguments.arguments("+380  95  201-11-33", false),
                Arguments.arguments("+38095192-21-12", false)
        };
    }

    @ParameterizedTest
    @MethodSource("isHexadecimalTestArgs")
    void isHexadecimalTest(String number, boolean expected){
        boolean actual = cut.isHexadecimal(number);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("isPhoneNumberTestArgs")
    void isPhoneNumberTest(String number, boolean expected){
        boolean actual = cut.isPhoneNumber(number);
        Assertions.assertEquals(expected, actual);
    }
}
