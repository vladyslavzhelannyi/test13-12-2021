package test.java.com.slice.utils.countUtils.countUtilityImpl;

import main.java.com.slice.utils.countUtils.countUtilityImpl.MultiplicationUtility;
import main.java.com.slice.utils.countUtils.countUtilityImpl.SubtractionUtility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class SubtractionUtilityTest {
    private static SubtractionUtility cut = new SubtractionUtility();

    static Arguments[] countTwoValueMultiplicationTestArgs(){
        return new Arguments[]{
                Arguments.arguments(32.45, 14.41, 18.04, 0.000001),
                Arguments.arguments(-345.12, -32.78, -312.34, 0.000001)
        };
    }

    @ParameterizedTest
    @MethodSource("countTwoValueMultiplicationTestArgs")
    void countTwoValueMultiplicationTest(double var1, double var2, double expected, double error){
        double actual = cut.countTwoValue(var1, var2);
        Assertions.assertEquals(expected, actual, error);
    }
}
