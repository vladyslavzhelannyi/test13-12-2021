package test.java.com.slice.utils.countUtils.countUtilityImpl;

import main.java.com.slice.utils.countUtils.countUtilityImpl.DivisionUtility;
import main.java.com.slice.utils.countUtils.countUtilityImpl.MultiplicationUtility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class MultiplicationUtilityTest {
    private static MultiplicationUtility cut = new MultiplicationUtility();

    static Arguments[] countTwoValueMultiplicationTestArgs(){
        return new Arguments[]{
                Arguments.arguments(1.31, 2.13, 2.7903, 0.000001),
                Arguments.arguments(-12.01, 4.92, -59.0892, 0.000001)
        };
    }

    @ParameterizedTest
    @MethodSource("countTwoValueMultiplicationTestArgs")
    void countTwoValueMultiplicationTest(double var1, double var2, double expected, double error){
        double actual = cut.countTwoValue(var1, var2);
        Assertions.assertEquals(expected, actual, error);
    }
}
