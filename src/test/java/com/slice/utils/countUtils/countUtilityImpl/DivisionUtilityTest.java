package test.java.com.slice.utils.countUtils.countUtilityImpl;

import main.java.com.slice.utils.countUtils.countUtilityImpl.AdditionUtility;
import main.java.com.slice.utils.countUtils.countUtilityImpl.DivisionUtility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class DivisionUtilityTest {
    private static DivisionUtility cut = new DivisionUtility();

    static Arguments[] countTwoValueDivisionTestArgs(){
        return new Arguments[]{
                Arguments.arguments(6.0, 1.5, 4.0, 0.000001),
                Arguments.arguments(361.872, 0, -1.0, 0.000001)
        };
    }

    @ParameterizedTest
    @MethodSource("countTwoValueDivisionTestArgs")
    void countTwoValueDivisionTest(double var1, double var2, double expected, double error){
        double actual = cut.countTwoValue(var1, var2);
        Assertions.assertEquals(expected, actual, error);
    }
}
