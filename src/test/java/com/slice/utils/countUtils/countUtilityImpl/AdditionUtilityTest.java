package test.java.com.slice.utils.countUtils.countUtilityImpl;

import main.java.com.slice.utils.countUtils.countUtilityImpl.AdditionUtility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class AdditionUtilityTest {
    private static AdditionUtility cut = new AdditionUtility();

    static Arguments[] countTwoValueAdditionTestArgs(){
        return new Arguments[]{
            Arguments.arguments(3.3, 4.4, 7.7, 0.000001),
            Arguments.arguments(-0.9, 13.56, 12.66, 0.000001)
        };
    }

    @ParameterizedTest
    @MethodSource("countTwoValueAdditionTestArgs")
    void countTwoValueAdditionTest(double var1, double var2, double expected, double error){
        double actual = cut.countTwoValue(var1, var2);
        Assertions.assertEquals(expected, actual, error);
    }
}
