package test.java.com.slice.utils.sortUtils;

import main.java.com.slice.models.Train;
import main.java.com.slice.utils.sortUtils.TrainSortingUtility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class TrainSortingUtilityTest {
    private static TrainSortingUtility cut = new TrainSortingUtility();

    private static Train train1 = new Train("a", 1, 2, 2);
    private static Train train2 = new Train("b", 2, 1, 1);
    private static Train train3 = new Train("b", 3, 3, 1);
    private static Train train4 = new Train("a", 1, 1, 2);
    private static Train train5 = new Train("c", 2, 4, 2);
    private static Train train6 = new Train("a", 1, 1, 1);
    private static Train train7 = new Train("c", 3, 2, 3);
    private static Train train8 = new Train("a", 1, 3, 1);
    private static Train train9 = new Train("c", 3, 4, 4);
    private static Train train10 = new Train("a", 1, 1, 4);

    private static Train[] trains1 = {train1, train2, train3, train4, train5};
    private static Train[] trains2 = {train6, train7, train8, train9, train10};

    static Arguments[] sortTrainsTestArgs(){
        return new Arguments[]{
          Arguments.arguments(trains1, new Train[]{train2, train3, train4, train1, train5}),
          Arguments.arguments(trains2, new Train[]{train6, train8, train7, train10, train9})
        };
    }

    @ParameterizedTest
    @MethodSource("sortTrainsTestArgs")
    void sortTrainsTest(Train[] input, Train[] expected){
        Train[] actual = cut.sortTrains(input);
        Assertions.assertArrayEquals(expected, actual);
    }
}
