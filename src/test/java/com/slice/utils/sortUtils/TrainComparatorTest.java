package test.java.com.slice.utils.sortUtils;

import main.java.com.slice.models.Train;
import main.java.com.slice.utils.sortUtils.TrainComparator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class TrainComparatorTest {
    private static TrainComparator cut = new TrainComparator();

    private static Train train1 = new Train("a", 1, 2, 1);
    private static Train train2 = new Train("b", 2, 1, 3);
    private static Train train3 = new Train("b", 1, 2, 1);
    private static Train train4 = new Train("a", 1, 3, 1);

    static Arguments[] compareTestArgs(){
        return new Arguments[]{
          Arguments.arguments(train1, train2, -2),
          Arguments.arguments(train1, train3, -1),
          Arguments.arguments(train1, train4, -1)
        };
    }

    @ParameterizedTest
    @MethodSource("compareTestArgs")
    void compareTest(Train train1, Train train2, int expected){
        int actual = cut.compare(train1, train2);
        Assertions.assertEquals(expected, actual);
    }
}
