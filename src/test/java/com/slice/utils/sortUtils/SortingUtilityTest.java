package test.java.com.slice.utils.sortUtils;

import main.java.com.slice.utils.sortUtils.SortingUtility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class SortingUtilityTest {
    private static SortingUtility cut = new SortingUtility();

    static Arguments[] sortArrayBubbleTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{104, -34, 104, 5, 0, -9}, new int[]{-34, -9, 0, 5, 104, 104}),
                Arguments.arguments(new int[]{543}, new int[]{543}),
                Arguments.arguments(new int[]{}, new int[]{})
        };
    }

    @ParameterizedTest
    @MethodSource("sortArrayBubbleTestArgs")
    public void sortArrayBubbleTest1(int[] array, int[] expected) {
        int[] actual = cut.sortArrayBubble(array);
        Assertions.assertArrayEquals(expected, actual);
    }
}
