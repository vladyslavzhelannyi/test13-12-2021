package test.java.com.slice.caches;

import main.java.com.slice.caches.CountUtilityFactoryCache;
import main.java.com.slice.utils.countUtils.ICountUtility;
import main.java.com.slice.utils.countUtils.countUtilityImpl.AdditionUtility;
import main.java.com.slice.utils.countUtils.countUtilityImpl.DivisionUtility;
import main.java.com.slice.utils.countUtils.countUtilityImpl.MultiplicationUtility;
import main.java.com.slice.utils.countUtils.countUtilityImpl.SubtractionUtility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

public class CountUtilityFactoryCacheTest {
    private static final String ADDITION = "addition";
    private static final String DIVISION = "division";
    private static final String MULTIPLICATION = "multiplication";
    private static final String SUBTRACTION = "subtraction";

    private static AdditionUtility additionUtility = Mockito.mock(AdditionUtility.class);
    private static DivisionUtility divisionUtility = Mockito.mock(DivisionUtility.class);
    private static MultiplicationUtility multiplicationUtility = Mockito.mock(MultiplicationUtility.class);
    private static SubtractionUtility subtractionUtility = Mockito.mock(SubtractionUtility.class);

    CountUtilityFactoryCache cut = new CountUtilityFactoryCache(additionUtility, divisionUtility, multiplicationUtility,
            subtractionUtility);

    static Arguments[] getCountUtilityArgs(){
        return new Arguments[]{
          Arguments.arguments(ADDITION, additionUtility),
          Arguments.arguments(DIVISION, divisionUtility),
          Arguments.arguments(MULTIPLICATION, multiplicationUtility),
          Arguments.arguments(SUBTRACTION, subtractionUtility)
        };
    }

    @ParameterizedTest
    @MethodSource("getCountUtilityArgs")
    void getCountUtility(String operation, ICountUtility expected){
        ICountUtility actual = cut.getCountUtility(operation);
        Assertions.assertEquals(expected, actual);
    }
}
