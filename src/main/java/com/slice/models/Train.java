package main.java.com.slice.models;

import java.util.Objects;

public class Train {
    private String destination;
    private int trainNumber;
    private double departureTime;
    private int placesNumber;

    public Train(){

    }

    public Train(String destination, int trainNumber, double departureTime, int placesNumber) {
        this.destination = destination;
        this.trainNumber = trainNumber;
        this.departureTime = departureTime;
        this.placesNumber = placesNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Train train = (Train) o;
        return trainNumber == train.trainNumber && Double.compare(train.departureTime, departureTime) == 0 && placesNumber == train.placesNumber && Objects.equals(destination, train.destination);
    }

    @Override
    public int hashCode() {
        return Objects.hash(destination, trainNumber, departureTime, placesNumber);
    }

    @Override
    public String toString() {
        return "Train: " +
                "destination = '" + destination + "'" +
                ", trainNumber = " + trainNumber +
                ", departureTime = " + departureTime +
                ", placesNumber = " + placesNumber + ".";
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(int trainNumber) {
        this.trainNumber = trainNumber;
    }

    public double getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(double departureTime) {
        this.departureTime = departureTime;
    }

    public int getPlacesNumber() {
        return placesNumber;
    }

    public void setPlacesNumber(int placesNumber) {
        this.placesNumber = placesNumber;
    }
}
