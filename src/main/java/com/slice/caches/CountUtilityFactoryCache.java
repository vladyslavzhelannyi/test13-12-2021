package main.java.com.slice.caches;

import main.java.com.slice.utils.countUtils.ICountUtility;
import main.java.com.slice.utils.countUtils.countUtilityImpl.AdditionUtility;
import main.java.com.slice.utils.countUtils.countUtilityImpl.DivisionUtility;
import main.java.com.slice.utils.countUtils.countUtilityImpl.MultiplicationUtility;
import main.java.com.slice.utils.countUtils.countUtilityImpl.SubtractionUtility;

public class CountUtilityFactoryCache {
    private static final String ADDITION = "addition";
    private static final String DIVISION = "division";
    private static final String MULTIPLICATION = "multiplication";
    private static final String SUBTRACTION = "subtraction";

    private ICountUtility additionUtility;
    private ICountUtility divisionUtility;
    private ICountUtility multiplicationUtility;
    private ICountUtility subtractionUtility;

    public CountUtilityFactoryCache(AdditionUtility additionUtility, DivisionUtility divisionUtility,
                                    MultiplicationUtility multiplicationUtility, SubtractionUtility subtractionUtility){
        this.additionUtility = additionUtility;
        this.divisionUtility = divisionUtility;
        this.multiplicationUtility = multiplicationUtility;
        this.subtractionUtility = subtractionUtility;
    }

    public ICountUtility getCountUtility(String operation){
        switch (operation){
            case ADDITION:
                return additionUtility;
            case DIVISION:
                return divisionUtility;
            case MULTIPLICATION:
                return multiplicationUtility;
            case SUBTRACTION:
                return subtractionUtility;
            default:
                return null;
        }
    }
}
