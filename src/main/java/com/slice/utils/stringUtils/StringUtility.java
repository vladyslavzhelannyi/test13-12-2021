package main.java.com.slice.utils.stringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtility {
    public boolean isHexadecimal(String number){
        boolean result = false;
        String hexPattern = "0x[0-9A-F]+";
        Pattern pattern = Pattern.compile(hexPattern);
        Matcher matcher = pattern.matcher(number);

        if (matcher.matches()){
            result = true;
        }
        return result;
    }

    public boolean isPhoneNumber(String phoneNumber){
        boolean result = false;
        String phonePattern = "\\+380\\s\\d{2}\\s(\\d){3}(-\\d{2}){2}";
        Pattern pattern = Pattern.compile(phonePattern);
        Matcher matcher = pattern.matcher(phoneNumber);
        if (matcher.matches()){
            result = true;
        }
        return result;
    }
}
