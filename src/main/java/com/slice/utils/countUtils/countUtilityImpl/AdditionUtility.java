package main.java.com.slice.utils.countUtils.countUtilityImpl;

import main.java.com.slice.utils.countUtils.ICountUtility;

public class AdditionUtility implements ICountUtility {
    @Override
    public double countTwoValue(double var1, double var2) {
        double result = var1 + var2;
        return result;
    }
}
