package main.java.com.slice.utils.sortUtils;

import main.java.com.slice.models.Train;

import java.util.Comparator;

public class TrainComparator implements Comparator<Train> {
//    Написать метод для сортировки Train по числу мест, если число мест одинаково - тогда отсортировать и по пункту
//    назначения, если и число мест и пункт назначения одинаковы, тогда если и пункт назначения одинаковый - время отправления.

    @Override
    public int compare(Train o1, Train o2) {
        int result = 0;
        result = o1.getPlacesNumber() - o2.getPlacesNumber();
        if (result == 0){
            result = o1.getDestination().compareTo(o2.getDestination());
        }
        if (result == 0){
            result = (int) (o1.getDepartureTime() - o2.getDepartureTime());
        }
        return result;
    }
}
