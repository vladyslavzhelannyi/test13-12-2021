package main.java.com.slice.utils.sortUtils;

import main.java.com.slice.models.Train;

import java.util.Arrays;

public class TrainSortingUtility {
    public Train[] sortTrains(Train[] trains){
        Arrays.sort(trains, new TrainComparator());
        return trains;
    }
}
