package main.java.com.slice.utils.sortUtils;

public class SortingUtility {
    public int[] sortArrayBubble(int[] array){
        boolean notSorted = true;
        while (notSorted) {
            notSorted = false;
            for(int i = 1; i < array.length; i++) {
                if (array[i] < array[i - 1]) {
                    int x = array[i];
                    array[i] = array[i - 1];
                    array[i - 1] = x;
                    notSorted = true;
                }
            }
        }
        return array;
    }
}
